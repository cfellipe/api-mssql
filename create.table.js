const connStr = 'mssql://sa:x@localhost:1433/node';
const sql = require("mssql");

sql.connect(connStr)
   .then(conn => createTable(conn))
   .catch(err => console.log("erro! " + err));

function createTable(conn) {
   const table = new sql.Table('Livros');
   table.create = true;
   table.columns.add('ID', sql.Int, {
      nullable: false,
      primary: true
   });
   table.columns.add('Nome', sql.NVarChar(150), {
      nullable: false
   });
   table.columns.add('ISBN', sql.NChar(20), {
      nullable: false
   });
   table.rows.add(1, 'Livro 01', '0001');
   table.rows.add(2, 'Livro 02', '0002');
   table.rows.add(3, 'Livro 03', '0003');

   const request = new sql.Request()
   
   request.bulk(table)
      .then(result => console.log('funcionou'))
      .catch(err => console.log('erro no bulk. ' + err));
}