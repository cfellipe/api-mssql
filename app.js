const express = require('express'),
    app = express(),
    livrosRouter = require('./src/controllers/livros.router'),
    connStr = 'mssql://sa:x@localhost:1433/node',
    sql = require("mssql"),
    bodyParser = require('body-parser')

sql.connect(connStr)
    .then(conn => GLOBAL.conn = conn)
    .catch(err => console.log(err));

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());
app.use('/livros', livrosRouter);

app.listen(3000, () => {
    console.log("Servidor Rodando na porta 3000");
})