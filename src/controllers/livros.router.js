const livrosController = require('./livros.controller');
let express = require('express');
let router = express.Router();

module.exports = router.get('/', livrosController.get)
    .get('/:id', livrosController.getByID)
    .post('/', livrosController.post);