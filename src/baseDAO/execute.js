class Execute {
    execSQLQuery(sqlQry, res){
        GLOBAL.conn.request()
                   .query(sqlQry)
                   .then(result => res.json(result.recordset))
                   .catch(err => res.json(err));
    }
}

module.exports =  new Execute();